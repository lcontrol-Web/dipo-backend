const Joi = require('joi');
const db = require("../modules/db");

class Tag {
    constructor(tag) {
        this.tagId = tag.tagId;
        this.tagName = tag.tagName;
        this.deviceId = tag.deviceId;
        this.objectType = tag.objectType;
        this.id = tag.id;
        this.indexArray = tag.indexArray;
        this.bitOffset = tag.bitOffset;
        this.amount = tag.amount;
        this.calculator = tag.calculator;
        this.timeSearch = tag.timeSearch;
        this.minValueToWrite = tag.minValueToWrite;
        this.maxValueToWrite = tag.maxValueToWrite;
        this.enableWriting = tag.enableWriting;
    }
}

Tag.validateTag = (tag) => {

    const schema = Joi.object({
        tagId: Joi.number().optional().allow(null),
        deviceName: Joi.string().optional().allow(null),
        tagName: Joi.string().max(256).required(),
        deviceId: Joi.number().required(),
        objectType: Joi.number().min(1).max(4).required(),
        id: Joi.number().required(),
        indexArray: Joi.number().min(1).required(),
        bitOffset: Joi.number().min(0).max(16).required(),
        amount: Joi.number().min(0).max(16).required(),
        calculator: Joi.string().max(45).required(),
        timeSearch: Joi.number().required(),
        minValueToWrite: Joi.number().required(),
        maxValueToWrite: Joi.number().required(),
        enableWriting: Joi.boolean().truthy(1).falsy(0).required(),
    });

    return schema.validate(tag);
}

Tag.getTags = async () => {
    const connection = await db.connection();
    const query = `SELECT tags.*, devices.deviceName
                   FROM tags, devices
                   WHERE tags.deviceId = devices.deviceId
                   ORDER BY id, indexArray, bitOffset, objectType;`;
    const tags = await connection.query(query);
    await connection.release();
    return tags;
}

Tag.getTagByTagId = async (tagId) => {
    const connection = await db.connection();
    const query = `SELECT tags.*, devices.deviceName  
                   FROM tags, devices
                   WHERE tagId = ?
                   AND tags.deviceId = devices.deviceId;`;
    const tag = await connection.query(query, tagId);
    await connection.release();
    return tag[0];
}

Tag.getTagByAddress = async (address) => {
    const connection = await db.connection();
    const query = `SELECT tags.*, devices.deviceName  
                   FROM tags, devices 
                   WHERE address = ?
                   AND tags.deviceId = devices.deviceId;`;
    const tag = await connection.query(query, address ? address : '');
    await connection.release();
    return tag[0];
}

Tag.getTagByDeviceIdAndIdAndIndexArrayAndBitOffsetAndObjectType = async (tag) => {
    let tempTag = new Tag(tag);
    const connection = await db.connection();
    const query = `SELECT *  
                   FROM tags 
                   WHERE deviceId = ? 
                   AND id = ? 
                   AND indexArray = ?
                   AND bitOffset = ?
                   AND objectType = ?;`;
    const values = [
        tempTag.deviceId,
        tempTag.id,
        tempTag.indexArray,
        tempTag.bitOffset,
        tempTag.objectType
    ];
    const result = await connection.query(query, values);
    await connection.release();
    return result[0];
}

Tag.create = async (tag) => {
    let addTag = new Tag(tag);
    const connection = await db.connection();
    const query = `INSERT INTO tags 
                    (tagName,
                     deviceId,
                     objectType,
                     id,
                     indexArray,
                     bitOffset,
                     amount,
                     calculator,
                     timeSearch,
                     minValueToWrite,
                     maxValueToWrite,
                     enableWriting) 
                 VALUES (?, ?, ?, ?,? , ?, ?, ?, ?, ?, ?, ?);`;

    const values = [
        addTag.tagName,
        addTag.deviceId,
        addTag.objectType,
        addTag.id,
        addTag.indexArray,
        addTag.bitOffset,
        addTag.amount,
        addTag.calculator,
        addTag.timeSearch,
        addTag.minValueToWrite,
        addTag.maxValueToWrite,
        addTag.enableWriting
    ];

    let result = await connection.query(query, values);
    await connection.release();
    return result;
}

Tag.update = async (tagId, tag) => {
    let updateTag = new Tag(tag);
    const connection = await db.connection();
    const query = `UPDATE tags 
                   SET 
                    tagName = ?, 
                    deviceId = ?,
                    objectType = ?,
                    id = ?,
                    indexArray = ?,
                    bitOffset = ?,
                    amount = ?,
                    calculator = ?,
                    timeSearch = ?,
                    minValueToWrite = ?,
                    maxValueToWrite = ?,
                    enableWriting = ? 
                   WHERE tagId = ?;`;

    const values = [
        updateTag.tagName,
        updateTag.deviceId,
        updateTag.objectType,
        updateTag.id,
        updateTag.indexArray,
        updateTag.bitOffset,
        updateTag.amount,
        updateTag.calculator,
        updateTag.timeSearch,
        updateTag.minValueToWrite,
        updateTag.maxValueToWrite,
        updateTag.enableWriting,
        tagId
    ];
    const result = await connection.query(query, values);
    await connection.release();
    return result;
}

Tag.delete = async (tagId) => {
    const connection = await db.connection();
    const query = `DELETE FROM tags 
                   WHERE tagId = ?`;
    const values = [tagId];
    const result = await connection.query(query, values);
    await connection.release();
    return result;
}

// async function a() {
//     let address = 1;
//     let tagId = 167131;

//     for (let i = 1; i < 500; i++) {
//         const connection = await db.connection();
//         const query = `UPDATE tags 
//         SET 
//         address = ${address}
//         WHERE deviceId = 1233
//         AND objectType = 4
//         AND tagId = ${tagId};`;

//         const result = await connection.query(query);
//         await connection.release();
//         address = address + 1;
//         tagId = tagId + 1;
//     }
// }

// a();

module.exports = Tag;