class Utils {
    constructor() {

    }
}

Utils.toCamel = function (o) {
    var newO, origKey, newKey, value
    if (o instanceof Array) {
        return o.map(function (value) {
            if (typeof value === "object") {
                value = toCamel(value)
            }
            return value
        })
    } else {
        newO = {}
        for (origKey in o) {
            if (o.hasOwnProperty(origKey)) {
                newKey = (origKey.charAt(0).toLowerCase() + origKey.slice(1) || origKey).toString()
                value = o[origKey]
                if (value instanceof Array || (value !== null && value.constructor === Object)) {
                    value = toCamel(value)
                }
                newO[newKey] = value
            }
        }
    }

    return newO;
}

Utils.getObjectTypeString = function (objectType) {
    switch (+objectType) {
        case 1:
            return 'Analog input';
        case 2:
            return 'Analog output';
        case 3:
            return 'Digital input';
        case 4:
            return 'Digital output';
        default:
            break;
    }
}



module.exports = Utils;