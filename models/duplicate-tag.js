const Joi = require('joi');

class DuplicateTag {
    constructor(duplicateTag) {
        this.tagId = duplicateTag.tagId;
        this.amount = duplicateTag.amount;
        this.startFrom = duplicateTag.startFrom;
        this.duplicateBy = +duplicateTag.duplicateBy;
        this.steps = duplicateTag.steps;
    }
}

DuplicateTag.validateDuplicateTag = (duplicateTag) => {

    const schema = Joi.object({
        tagId: Joi.number().required(),
        amount: Joi.number().min(1).max(99999).required(),
        startFrom: Joi.number().min(1).required(),
        duplicateBy: Joi.number().min(1).max(4).required(),
        steps: Joi.number().min(1).max(4).required(),
    });

    return schema.validate(duplicateTag);
}

module.exports = DuplicateTag;
