const Joi = require('joi');
const db = require("../modules/db");

class ModbusTag {
    constructor(modbusTag) {
        this.tagId = modbusTag.tagId;
        this.address = modbusTag.address;
    }
}

ModbusTag.validateModbusTag = (modbusTag) => {

    const schema = Joi.object({
        tagId: Joi.number().required(),
        address: Joi.number().min(1).optional().allow(null).allow(''),
    });

    return schema.validate(modbusTag);
}

ModbusTag.getModbusTags = async () => {
    const connection = await db.connection();
    const query = `SELECT tags.deviceId, tags.tagId, tags.tagName, tags.objectType, 
                          tags.id, tags.indexArray, tags.bitOffset, 
                          tags.address, tags.address AS realAddress, devices.deviceName
                   FROM tags, devices
                   WHERE tags.deviceId = devices.deviceId
                   ORDER BY tags.id, tags.indexArray, tags.bitOffset, tags.objectType, tags.tagId;`;
    const modbusTags = await connection.query(query);
    await connection.release();
    return modbusTags;
}

ModbusTag.update = async (modbusTag) => {
    let updateModbusTag = new ModbusTag(modbusTag);
    const connection = await db.connection();
    const query = `UPDATE tags 
                   SET 
                    address = ?
                   WHERE tagId = ?;`;

    const values = [
        updateModbusTag.address,
        updateModbusTag.tagId
    ];
    const result = await connection.query(query, values);
    await connection.release();
    return result;
}

module.exports = ModbusTag;