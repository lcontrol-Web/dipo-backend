const db = require("../modules/db");

class Log {
    constructor(log) {
        this.tagId = log.tagId;
        this.value = log.value;
        this.time = log.time;
    }
}

Log.getLogs = async () => {
    const connection = await db.connection();
    const query = `SELECT logs.*, tags.tagName, tags.deviceId, tags.objectType, tags.id, 
                          tags.indexArray, tags.bitOffset, devices.deviceName 
                   FROM logs, tags, devices
                   WHERE logs.tagId = tags.tagId
                   AND devices.deviceId = tags.deviceId
                   ORDER BY logId DESC;`;
    const logs = await connection.query(query);
    await connection.release();
    return logs;
}

Log.getLogByLogId = async (logId) => {
    const connection = await db.connection();
    const query = `SELECT logs.*, tags.tagName, tags.deviceId, tags.objectType, tags.id, 
                          tags.indexArray, tags.bitOffset, devices.deviceName 
                   FROM logs, tags, devices
                   WHERE logs.logId = ?
                   AND logs.tagId = tags.tagId 
                   AND devices.deviceId = tags.deviceId;`;
    const logs = await connection.query(query, logId);
    await connection.release();
    return logs[0];
}

Log.deleteLogsByDayDifference = async (numOfDays) => {
    const connection = await db.connection();
    const query = `DELETE FROM logs 
                   WHERE time <= DATE_SUB(NOW() , INTERVAL ${numOfDays} DAY)`;
    const logs = await connection.query(query);
    await connection.release();
    return logs[0];
}

module.exports = Log;