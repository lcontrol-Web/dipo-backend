const Joi = require('joi');
const db = require("../modules/db");

class Device {
    constructor(device) {
        this.deviceId = +device.deviceId;
        this.deviceName = device.deviceName;
        this.ip = device.ip;
        this.port = device.port;
        this.timeSearch = device.timeSearch;
        this.timeOut = device.timeOut;
        this.protocol = device.protocol;
        this.maxRegisterToRead = device.maxRegisterToRead;
        this.enable = device.enable;
    }
}

Device.validateDevice = (device) => {

    const schema = Joi.object({
        deviceId: Joi.number().optional().allow(null),
        deviceName: Joi.string().min(1).max(128).required(),
        ip: Joi.string().pattern(new RegExp(/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/)).min(1).max(128).required(),
        port: Joi.number().min(1).max(65535).required(),
        timeSearch: Joi.number().required(),
        timeOut: Joi.number().required(),
        protocol: Joi.string().min(1).max(128).required(),
        maxRegisterToRead: Joi.number().min(1).max(128).required(),
        enable: Joi.boolean().truthy(1).falsy(0).required(),
        status: Joi.boolean().truthy(1).falsy(0).optional().allow(null)
    });

    return schema.validate(device);
}

Device.getDevices = async () => {
    const connection = await db.connection();
    const query = `SELECT *
                   FROM devices
                   ORDER BY INET_ATON(ip);`;
    const devices = await connection.query(query);
    await connection.release();
    return devices;
}

Device.getDeviceByDeviceId = async (deviceId) => {
    const connection = await db.connection();
    const query = `SELECT *  
                   FROM devices 
                   WHERE deviceId = ?;`;
    const devices = await connection.query(query, deviceId);
    await connection.release();
    return devices[0];
}

Device.getDeviceByDeviceName = async (deviceName) => {
    const connection = await db.connection();
    const query = `SELECT *  
                   FROM devices 
                   WHERE deviceName = ?;`;
    const device = await connection.query(query, deviceName);
    await connection.release();
    return device[0];
}

Device.create = async (device) => {
    let addDevice = new Device(device);
    const connection = await db.connection();
    const query = `INSERT INTO devices 
                    (deviceName, 
                     ip,
                     port,
                     timeSearch,
                     timeOut,
                     protocol,
                     maxRegisterToRead,
                     enable) 
                 VALUES (?, ?, ?, ?, ?, ?, ?, ?);`;

    const values = [
        addDevice.deviceName,
        addDevice.ip,
        addDevice.port,
        addDevice.timeSearch,
        addDevice.timeOut,
        addDevice.protocol,
        addDevice.maxRegisterToRead,
        addDevice.enable
    ];

    let result = await connection.query(query, values);
    await connection.release();
    return result;
}

Device.update = async (deviceId, device) => {
    let updateDevice = new Device(device);

    const connection = await db.connection();
    const query = `UPDATE devices 
                   SET 
                    deviceName = ?, 
                    ip = ?,
                    port = ?,
                    timeSearch = ?,
                    timeOut = ?,
                    protocol = ?,
                    maxRegisterToRead = ?,
                    enable = ?
                   WHERE deviceId = ?;`;

    const values = [
        updateDevice.deviceName,
        updateDevice.ip,
        updateDevice.port,
        updateDevice.timeSearch,
        updateDevice.timeOut,
        updateDevice.protocol,
        updateDevice.maxRegisterToRead,
        updateDevice.enable,
        deviceId
    ];
    const result = await connection.query(query, values);
    await connection.release();
    return result;
}

Device.delete = async (deviceId) => {
    const connection = await db.connection();
    const query = `DELETE FROM devices 
                   WHERE deviceId = ?`;
    const values = [deviceId];
    const result = await connection.query(query, values);
    await connection.release();
    return result;
}

module.exports = Device;