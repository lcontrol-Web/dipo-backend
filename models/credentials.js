const Joi = require('joi');

class Credentials {
    constructor(credentials) {
        this.userName = credentials.userName;
        this.password = credentials.password;
    }
}

Credentials.validateCredentials = (credentials) => {

    const schema = Joi.object({
        userName: Joi.string().min(1).max(128).required(),
        password: Joi.string().min(4).max(128).required(),
        remember: Joi.boolean().optional()
    });

    return schema.validate(credentials);
}

module.exports = Credentials;