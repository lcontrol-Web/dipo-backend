const Joi = require('joi');
const jwt = require('jsonwebtoken');
const db = require("../modules/db");

class User {
    constructor(user) {
        this.userId = user.userId;
        this.userName = user.userName;
        this.password = user.password;
    }

    generateToken = function () {
        const token = jwt.sign({
            userId: this.userId,
            userName: this.userName,
        }, process.env.JWT_PRIVATE_KEY, {
            expiresIn: '31d'
        });

        return token;
    }
}

User.validateUser = (user) => {

    const schema = Joi.object({
        userId: Joi.string().optional().allow(null),
        userName: Joi.string().min(1).max(128).required(),
        password: Joi.string().min(1).max(128).required(),
    });

    return schema.validate(user);
}

User.getAllUsers = async () => {

    const connection = await db.connection();
    const query = `SELECT *
                   FROM users
                   ORDER BY userId DESC;`;
    const users = await connection.query(query);
    await connection.release();
    return users;
}

User.getUserByUserId = async (userId) => {
    const connection = await db.connection();
    const query = `SELECT *  
                   FROM users 
                   WHERE userId = ?;`;
    const user = await connection.query(query, userId);
    await connection.release();
    return user;
}

User.getUserByUserName = async (userName) => {
    const connection = await db.connection();
    const query = `SELECT *  
                   FROM users 
                   WHERE userName = ?;`;
    const user = await connection.query(query, userName);
    await connection.release();
    return user;
}

User.getUserWithAllFieldsByUserId = async (userId) => {

    const connection = await db.connection();
    const query = `SELECT * 
                   FROM users 
                   WHERE userId = ?;`;
    const user = await connection.query(query, userId);
    await connection.release();
    return user;
}

User.create = async (user) => {
    const connection = await db.connection();
    const query = `INSERT INTO users 
                    (userName, 
                     password) 
                 VALUES (?, ?);`;

    const values = [
        user.userName,
        user.password
    ];

    let result = await connection.query(query, values);
    await connection.release();
    return result;
}

User.update = async (user) => {
    let updateUser = new User(user);

    const connection = await db.connection();
    const query = `UPDATE users 
                   SET 
                    userName = ?, 
                    password = ?
                   WHERE userId = ?;`;

    const values = [
        updateUser.userName,
        updateUser.password,
        updateUser.userId
    ];
    const result = await connection.query(query, values);
    await connection.release();
    return result;
}

User.delete = async (userId) => {
    const connection = await db.connection();
    const query = `DELETE FROM users 
                   WHERE userId = ?`;
    const values = [userId];
    const result = await connection.query(query, values);
    await connection.release();
    return result;
}

module.exports = User;