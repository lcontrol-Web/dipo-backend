const express = require('express');
require('express-async-errors');
const app = express();
const cors = require('cors');
const logger = require('./utils/logger');
const morgan = require('morgan');
require('dotenv').config({
    path: __dirname + '/.env'
});

app.use(express.json({
    limit: '50mb'
}));

app.use(express.urlencoded({
    limit: '50mb',
    extended: true
}));

// allow access from other services
app.use(cors());
app.use((req, res, next) => {
    res.setHeader('Access-Control-Expose-Headers', 'x-auth-token');
    next();
});

if (process.env.NODE_ENV != 'production')
    app.use(morgan('dev'));

require('./startup/config')();
require('./startup/routes')(app);
require('./startup/prod')(app);

const port = process.env.PORT;
const server = app.listen(port, () => {
    logger.info(`Listening on port ${port}`);
    startDeleteLogsInterval();
});

async function startDeleteLogsInterval() {
    const Log = require('./models/log');
    await Log.deleteLogsByDayDifference(60);
    setInterval(async () => {
        await Log.deleteLogsByDayDifference(60);
    }, 3600000);
}

module.exports = server;