const jwt = require('jsonwebtoken');

// for protecting the routes that can make a change in the database
module.exports = function (req, res, next) {

    // get the token from the header that the user sent
    const token = req.header('x-auth-token');

    // the request does not have a token
    if (!token)
        return res.status(401).send('Access denied. No token provided.');

    try {
        // verify that the digital signutare from the token is the as our enviroment variable
        // if the token is invalid it will throw an exception
        const decoded = jwt.verify(token, process.env.JWT_PRIVATE_KEY);
        req.user = decoded;
        next();
    } catch (ex) {

        if (ex.name === 'TokenExpiredError')
            res.status(400).send('ExpiredToken');
        else
            res.status(400).send('Invalid token.');

    }
}