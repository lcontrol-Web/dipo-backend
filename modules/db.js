const mysql = require('mysql');
const logger = require('../utils/logger');

const pool = mysql.createPool({
    connectionLimit: 100,
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASS,
    multipleStatements: true,
    database: 'dipo'
});

const connection = () => {
    return new Promise((resolve, reject) => {
        pool.getConnection((err, connection) => {
            if (err) {
                logger.error('MySQL pool cannot connect ' + err.message);
                reject(err);
                return;
            }

            if (process.env.NODE_ENV != 'production')
                console.log("MySQL pool connected: threadId " + connection.threadId);

            const query = (sql, binding) => {
                return new Promise((resolve, reject) => {
                    connection.query(sql, binding, (err, result) => {
                        if (err) {
                            logger.error(err);
                            connection.release();
                            reject(err);
                            return;
                        }

                        resolve(result);
                    });
                });
            };

            const release = () => {
                return new Promise((resolve, reject) => {
                    if (err) {
                        logger.error(err);
                        reject(err);
                        return;
                    }

                    if (process.env.NODE_ENV != 'production')
                        console.log("MySQL pool released: threadId " + connection.threadId);

                    resolve(connection.release());
                });
            };

            const escapeSansQuotes = (string) => {
                return connection.escape(string).match(/^'(\w+)'$/)[1];
            };

            const escape = (string) => {
                return connection.escape(string);
            };

            resolve({
                query,
                release,
                escape,
                escapeSansQuotes
            });
        });
    });
};

const query = (sql, binding) => {
    return new Promise((resolve, reject) => {
        pool.query(sql, binding, (err, result, fields) => {
            if (err) {
                logger.error(err);
                reject(err);
                return;
            }

            resolve(result);
        });
    });
};

const end = () => {
    return new Promise((resolve, reject) => {

        pool.end(function (err) {
            if (err) {
                reject(err);
                return;
            }

            if (process.env.NODE_ENV != 'production')
                console.log("MySQL released all pools");
            resolve();
        });
    });
}
const escape = (string) => {
    return this.connection.escape(string);
}

module.exports = {
    connection,
    query,
    end,
    escape
};