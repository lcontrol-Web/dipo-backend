const express = require('express');
const auth = require('../middleware/auth');
const Tag = require('../models/tag');
const DuplicateTag = require('../models/duplicate-tag');
const Device = require('../models/device');
const utils = require('../models/utils');
const router = express.Router();

router.get('/', [auth], async (req, res) => {
    const tags = await Tag.getTags();
    res.send(tags);
});

router.get('/:id', [auth], async (req, res) => {

    if (!req.params.id)
        return res.status(400).send('tag ID is required');

    const tag = await Tag.getTagByTagId(req.params.id);
    res.send(tag);
});

router.post('/', [auth], async (req, res) => {

    const {
        error
    } = Tag.validateTag(req.body);
    if (error)
        return res.status(400).send(error.details[0].message);

    let tag = new Tag(req.body);
    const foundedTag = await Tag.getTagByDeviceIdAndIdAndIndexArrayAndBitOffsetAndObjectType(tag);
    if (foundedTag)
        return res.status(400).send('duplicated tag');

    const result = await Tag.create(tag);
    tag.tagId = result.insertId;
    res.send(tag);
});

router.put('/:id', [auth], async (req, res) => {

    if (!req.params.id)
        return res.status(400).send('tag ID is required');

    const {
        error
    } = Tag.validateTag(req.body);
    if (error)
        return res.status(400).send(error.details[0].message);

    const tagId = +req.params.id;
    const foundedTagById = await Tag.getTagByTagId(tagId);
    if (!foundedTagById)
        return res.status(400).send('the tag with the given ID not found');

    const tag = new Tag(req.body);

    const foundedTag = await Tag.getTagByDeviceIdAndIdAndIndexArrayAndBitOffsetAndObjectType(tag);
    if (foundedTag && foundedTag.tagId != tagId)
        return res.status(400).send('Duplicated tag');

    const result = await Tag.update(tagId, tag);
    tag.tagId = tagId;
    res.send(tag);
});

router.delete('/:id', [auth], async (req, res) => {

    if (!req.params.id)
        return res.status(400).send('tag ID is required');

    const tagId = +req.params.id;
    const tag = await Tag.getTagByTagId(tagId);
    if (!tag)
        return res.status(400).send('the tag with the given ID not exist');

    await Tag.delete(tagId);
    res.send(tag);
});

router.post('/duplicate', [auth], async (req, res) => {
    const {
        error
    } = DuplicateTag.validateDuplicateTag(req.body);
    if (error)
        return res.status(400).send(error.details[0].message);

    let duplicateTag = new DuplicateTag(req.body);
    const foundedTagById = await Tag.getTagByTagId(duplicateTag.tagId);
    if (!foundedTagById)
        return res.status(400).send('the tag with the given ID not found');

    const device = await Device.getDeviceByDeviceId(foundedTagById.deviceId);

    let start = duplicateTag.startFrom

    for (let i = 0; i < duplicateTag.amount; i++) {
        let tag = new Tag(foundedTagById);

        switch (duplicateTag.duplicateBy) {
            // index array
            case 1:
                tag.indexArray = start;
                break;
                // id
            case 2:
                tag.id = start;
                break;
                // bit offset
            case 3:
                tag.bitOffset = start;
                break;
            default:
                break;
        }
        const duplicatedTag = await Tag.getTagByDeviceIdAndIdAndIndexArrayAndBitOffsetAndObjectType(tag);
        if (!duplicatedTag) {
            tag.tagName = `${device.deviceName}_${utils.getObjectTypeString(tag.objectType)}_${tag.id}.${tag.indexArray}/${tag.bitOffset}`;
            await Tag.create(tag);
        }

        start += duplicateTag.steps;
    }

    res.send({
        success: true
    });
});

router.post('/import', [auth], async (req, res) => {

    const importTags = req.body;
    let importTagsError = [];
    const devices = await Device.getDevices();

    for (let importTag of importTags) {
        let tag = new Tag({});
        tag.tagName = importTag['FreeDescription'];
        tag.id = importTag['Id'];
        tag.indexArray = importTag['IndexArray'];
        tag.bitOffset = importTag['BitOffset'];
        tag.calculator = importTag['Calculator'];
        tag.timeSearch = importTag['TimeSearch'];
        tag.minValueToWrite = importTag['MinValue'];
        tag.maxValueToWrite = importTag['MaxValue'];
        tag.enableWriting = importTag['Write'] ? 1 : 0;

        const objectNameArray = importTag['ObjectName'].split("_");
        const objectType = objectNameArray[1].toLowerCase();
        switch (objectType) {
            case 'analog input tcpip':
                tag.objectType = 1;
                break;
            case 'analog output tcpip':
                tag.objectType = 2;
                break;
            case 'digital input tcpip':
                tag.objectType = 3;
                break;
            case 'digital output tcpip':
                tag.objectType = 4;
                break;
            default:
                tag.objectType = 1;
        }

        const deviceName = objectNameArray[0].toLowerCase();
        const device = devices.find(d => d.deviceName.toLowerCase() == deviceName);

        if (device)
            tag.deviceId = device.deviceId;
        else {
            importTagsError.push({
                tagName: importTag['FreeDescription'],
                deviceName: deviceName
            });
            continue;
        }

        const {
            error
        } = Tag.validateTag(tag);
        if (error) {
            importTagsError.push({
                tagName: importTag['FreeDescription'],
                deviceName: deviceName
            });
            continue;
        }

        const foundedTag = await Tag.getTagByDeviceIdAndIdAndIndexArrayAndBitOffsetAndObjectType(tag);
        if (foundedTag) {
            importTagsError.push({
                tagName: importTag['FreeDescription'],
                deviceName: deviceName
            });
            continue;
        } else
            await Tag.create(tag);
    }

    res.send(importTagsError);
});


module.exports = router;