const express = require('express');
const auth = require('../middleware/auth');
const Device = require('../models/device');
const ModbusTag = require('../models/modbusTag');
const Tag = require('../models/tag');
const router = express.Router();

router.get('/', [auth], async (req, res) => {
    const modbusTags = await ModbusTag.getModbusTags();
    res.send(modbusTags);
});

router.post('/', [auth], async (req, res) => {

    const modbusTags = req.body;
    let cannotUpdateModbusTags = [];

    for (const modbusTag of modbusTags) {

        const {
            error
        } = ModbusTag.validateModbusTag(modbusTag);
        if (error) {
            cannotUpdateModbusTags.push(modbusTag);
            continue;
        }

        const foundedTagById = await Tag.getTagByTagId(modbusTag.tagId);
        if (!foundedTagById) {
            cannotUpdateModbusTags.push(modbusTag);
            continue;
        }

        const foundedTagByAddress = await Tag.getTagByAddress(modbusTag.address);
        if (foundedTagByAddress && foundedTagByAddress.tagId != modbusTag.tagId) {
            cannotUpdateModbusTags.push(modbusTag);
            continue;
        }

        var result = await ModbusTag.update(modbusTag);
    }

    res.send(cannotUpdateModbusTags);
});

router.post('/import-address', [auth], async (req, res) => {

    const importAddressTags = req.body;
    let importAddressTagsError = [];
    const promise1 = Device.getDevices();
    const promise2 = ModbusTag.getModbusTags();
    const response = await Promise.all([promise1, promise2]);
    const devices = response[0];
    const tags = response[1];
    let deviceName;
    
    for (let importAddressTag of importAddressTags) {
        try {
            const objectNameArray = importAddressTag['ObjectName'].split("_");

            // check if device name exists
            deviceName = objectNameArray[0].toLowerCase();
            const device = devices.find(d => d.deviceName.toLowerCase() == deviceName);
            if (!device)
                throw new Error('Device name not exists');

            // get the object type
            let objectType = 1;
            switch (objectNameArray[1].toLowerCase()) {
                case 'analog input tcpip':
                    objectType = 1;
                    break;
                case 'analog output tcpip':
                    objectType = 2;
                    break;
                case 'digital input tcpip':
                    objectType = 3;
                    break;
                case 'digital output tcpip':
                    objectType = 4;
                    break;
                default:
                    objectType = 1;
            }

            const foundedTag = tags.filter(function (t) {
                if (t.deviceId == device.deviceId &&
                    t.objectType == objectType &&
                    t.id == importAddressTag['ID'] &&
                    t.indexArray == importAddressTag['IndexArray'])
                    return true;

                return false;
            });

            if (foundedTag.length == 1) {
                let address = importAddressTag['Ext. Modbus Address'];
                let modbusTag = new ModbusTag({
                    tagId: foundedTag[0].tagId,
                    address: address
                });
                await ModbusTag.update(modbusTag);
            } else
                throw new Error('Tag not found');
        } catch (error) {
            importAddressTagsError.push({
                tagName: importAddressTag['FreeDescription'],
                deviceName: deviceName
            });
            continue;
        }
    }

    res.send(importAddressTagsError);
});


module.exports = router;