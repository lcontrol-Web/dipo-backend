const express = require('express');
const auth = require('../middleware/auth');
const Device = require('../models/device');
const Utils = require('../models/utils');
const router = express.Router();

router.get('/', [auth], async (req, res) => {
    const devices = await Device.getDevices();
    res.send(devices);
});

router.get('/:id', [auth], async (req, res) => {

    if (!req.params.id)
        return res.status(400).send('device ID is required');

    const device = await Device.getDeviceByDeviceId(req.params.id);
    res.send(device);
});

router.post('/', [auth], async (req, res) => {

    const {
        error
    } = Device.validateDevice(req.body);
    if (error)
        return res.status(400).send(error.details[0].message);

    let device = new Device(req.body);

    const foundedDevice = await Device.getDeviceByDeviceName(device.deviceName);
    if (foundedDevice)
        return res.status(400).send('a device with the given name already exist');

    const result = await Device.create(device);
    device.deviceId = result.insertId;
    res.send(device);
});

router.put('/:id', [auth], async (req, res) => {

    if (!req.params.id)
        return res.status(400).send('device ID is required');

    const {
        error
    } = Device.validateDevice(req.body);
    if (error)
        return res.status(400).send(error.details[0].message);

    const deviceId = +req.params.id;
    let foundedDevice = await Device.getDeviceByDeviceId(deviceId)
    if (!foundedDevice)
        return res.status(400).send('the device with the given ID not found');

    const device = new Device(req.body);

    // check if device name already exist
    foundedDevice = await Device.getDeviceByDeviceName(device.deviceName);
    if (foundedDevice && foundedDevice.deviceId != deviceId)
        return res.status(400).send('a device with the given name already exist');

    const result = await Device.update(deviceId, device);
    device.deviceId = deviceId;
    res.send(device);
});

router.delete('/:id', [auth], async (req, res) => {

    if (!req.params.id)
        return res.status(400).send('device ID is required');

    const deviceId = +req.params.id;
    const foundedDevice = await Device.getDeviceByDeviceId(deviceId)
    if (!foundedDevice)
        return res.status(400).send('The device with the given ID not exist');

    const result = await Device.delete(deviceId);
    res.send(foundedDevice);
});

router.post('/import', [auth], async (req, res) => {
    const devices = req.body;
    let importDevicesError = [];

    for (let device of devices) {
        device = Utils.toCamel(device);
        const {
            error
        } = Device.validateDevice(device);
        if (error) {
            importDevicesError.push({
                deviceName: device.deviceName
            });
            continue;
        }

        // check if device name already exist
        const foundedDevice = await Device.getDeviceByDeviceName(device.deviceName);
        if (foundedDevice) {
            importDevicesError.push({
                deviceName: device.deviceName
            });
            continue;
        }

        await Device.create(device);
    }

    res.send(importDevicesError);
});

module.exports = router;