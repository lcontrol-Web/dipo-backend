const express = require('express');
const bcrypt = require('bcrypt');
const Credentials = require('../models/credentials');
const User = require('../models/user');
const router = express.Router();

router.post('/', async (req, res) => {

    const {
        error
    } = Credentials.validateCredentials(req.body);
    if (error)
        return res.status(400).send(error.details[0].message);

    let foundedUser = await User.getUserByUserName(req.body.userName);
    if (!foundedUser.length)
        return res.status(400).send('Invalid user name or password');

    let user = new User(foundedUser[0]);
    const validPassword = await bcrypt.compare(req.body.password, user.password);
    if (!validPassword)
        return res.status(400).send('Invalid user name or password');

    const token = user.generateToken();
    user.token = token;

    res.status(200).set('x-auth-token', token).send({
        userName: user.userName, 
        userId: user.userId, 
    });
});

module.exports = router;