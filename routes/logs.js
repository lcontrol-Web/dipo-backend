const express = require('express');
const auth = require('../middleware/auth');
const Log = require('../models/log');
const router = express.Router();

router.get('/', [auth], async (req, res) => {
    const logs = await Log.getLogs();
    res.send(logs);
});

router.get('/:id', [auth], async (req, res) => {

    if (!req.params.id)
        return res.status(400).send('log ID is required');

    const log = await Log.getLogByLogId(req.params.id);
    res.send(log);
});


module.exports = router;