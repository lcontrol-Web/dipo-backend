const auth = require('../routes/auth');
const logs = require('../routes/logs');
const tags = require('../routes/tags');
const devices = require('../routes/devices');
const modbusTags = require('../routes/modbusTags');
const error = require('../middleware/error');

module.exports = function (app) {
    app.use('/api/auth', auth);
    app.use('/api/logs', logs);
    app.use('/api/tags', tags);
    app.use('/api/devices', devices);
    app.use('/api/modbus-tags', modbusTags);

    // this middleware error function will activate after the routes above, by calling the next() function in the routes
    app.use(error);
};