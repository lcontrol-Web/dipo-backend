const helmet = require('helmet');
const compression = require('compression');

module.exports = function (app) {

    if (process.env.NODE_ENV === 'production') {

        // protect from attacks
        app.use(helmet());

        // decrease the size of the response body and hence increase the speed of a web app
        app.use(compression());
    } 
}